Ben Simpson
CS 360
Lab 9 - Angular

This repository contains code for my Lab 9 submission.  The URL for the repository is https://bitbucket.org/benssimpson/cs360-lab-9

My comments app can be found at http://ec2-54-88-82-132.compute-1.amazonaws.com:3000/

For the creative portion of my project, I created a text header that intercepts mouse events that occur to it.  You can see whether the mouse is currently located over the text element, and if it is the global mouse coordinates are displayed.  In addition, you can see the number of times that you have clicked the element.  The click count is reset whenever the page is reloaded.
