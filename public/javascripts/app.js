angular.module('comment', [])
.controller('MainCtrl', [
  '$scope','$http',
  function($scope,$http){
     $scope.comments = [];  
     $scope.addComment = function() { 
       if($scope.formContent === '') { return; } 
       console.log("In addComment with "+$scope.formContent); 
       $scope.create({ title: $scope.formContent, upvotes: 0, }); 
       $scope.formContent = '';
     };
     $scope.upvote = function(comment) { 
       return $http.put('/comments/' + comment._id + '/upvote') .success(function(data){ 
         console.log("upvote worked"); 
         comment.upvotes += 1; 
       }); 
     };
     $scope.incrementUpvotes = function(comment) { $scope.upvote(comment); };
     $scope.getAll = function() { 
       return $http.get('/comments').success(function(data){ 
         angular.copy(data, $scope.comments); 
       }); 
     };
     $scope.create = function(comment) { 
       return $http.post('/comments', comment).success(function(data){ 
         $scope.comments.push(data); 
       }); 
     };
     $scope.getAll();
     $scope.mouse = {
       over: false,
       x: 'N/A',
       y: 'N/A',
       clicks: 0
     }
     $scope.mouseMoveFunc = function(mEvent){
       if($scope.mouse.over == true)
       {
         $scope.mouse.x = mEvent.clientX;
         $scope.mouse.y = mEvent.clientY;
       }
       else
       {
         $scope.mouse.x = 'N/A';
         $scope.mouse.y = 'N/A';
       }
     }
     $scope.mouseUpFunc = function(mEvent)
     {
       $scope.mouse.clicks += 1;
     }
  }
]);

